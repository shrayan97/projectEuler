'''
Project Euler #3

The prime factors of 13195 are 5, 7, 13 and 29.
What is the largest prime factor of the number 600851475143?

Correct Answer = 6857
'''

'''
Start with 2 variables:

 - the number you want the largest prime of (numToFactor) (n)
 - the divisor, start at the first known prime, 2 (divisor) (d)

Try to divide n by d. If there is no remainder go ahead and divide
it. This makes the new n = n / d. If you are unable to divide n by d
evenly, increment d by 1 and start again. This algorithm doesn't really
care about finding primes, just the largest divisor, and by starting at 2
you can guarentee that the largest divisor you find will be prime.

This algorithm can be improved if we can make d the next known prime instead
of simply incrementing it. But this works fast enough.
'''
numToFactor = 600851475143
divisor = 2

while(numToFactor != 1):
    if(numToFactor % divisor == 0):
        numToFactor /= divisor
    else:
        divisor += 1

print(divisor)

