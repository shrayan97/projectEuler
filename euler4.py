'''
Project Euler #4

A palindromic number reads the same both ways.
The largest palindrome made from the product of two 2-digit numbers is
9009 = 91 × 99.
Find the largest palindrome made from the product of two 3-digit numbers.
'''

smallestDigit = 100
largestDigit = 999
currentPalindrome = -1
largestPalindrome = -1

for i in range(largestDigit, smallestDigit, -1):
    for j in range(largestDigit, smallestDigit, -1):
        if(str(i * j) == str(i * j)[::-1]):
            currentPalindrome = i * j
            if(currentPalindrome > largestPalindrome):
                largestPalindrome = currentPalindrome

print(largestPalindrome)
        
