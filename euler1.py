'''
Project Euler #1

If we list all the natural numbers below 10 that are multiples of 3 or 5,
we get 3, 5, 6 and 9. The sum of these multiples is 23.
Find the sum of all the multiples of 3 or 5 below 1000.

correct answer = 233168
'''

targetLength = 1000
runningSum = 0

for currentNumber in range(0,targetLength):
    if(currentNumber % 3 == 0 or currentNumber % 5 == 0):
        runningSum += currentNumber

print(runningSum)
    
    

