'''
Project Euler #5

2520 is the smallest number that can be divided by each of the numbers
from 1 to 10 without any remainder. What is the smallest positive number
that is evenly divisible by all of the numbers from 1 to 20?
'''

#only need the ones up till 20
primes = [2,3,5,7,11,13,17,19]
currentVal = 1 
for currentPrime in primes:
    expVal = 2
    currentVal *= currentPrime
    while (currentPrime**expVal < 21):
        currentVal *= currentPrime
        expVal += 1

print(currentVal)
