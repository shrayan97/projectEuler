'''
Project Euler #6

The sum of the squares of the first ten natural numbers is,
1**2 + 2**2 + ... + 10**2 = 385
The square of the sum of the first ten natural numbers is,
(1 + 2 + ... + 10)**2 = 55**2 = 3025

Hence the difference between the sum of the squares of the first
ten natural numbers and the square of the sum is 3025 − 385 = 2640.
Find the difference between the sum of the squares of the first
one hundred natural numbers and the square of the sum.

----------------------------------------------------------------

sum from 1 till n --> (n(n + 1))/2
sum from 1**2 till n**2 --> (n(n + 1)(2 * n + 1))/6

----------------------------------------------------------------

n is the end of range
simply change this to get the difference from 1 - n
'''

n = 100

sumOfSquares = (n * (n + 1) * (2 * n + 1))/6
squareOfSums = ((n * (n + 1))/2)**2
difference = squareOfSums - sumOfSquares

print(difference)
    
